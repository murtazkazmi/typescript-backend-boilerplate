import app from "./app";
import compression from "compression";
import helmet from "helmet";
import allRoutes from './routes'
import cors from 'cors'
import * as bodyParser from 'body-parser'
import morgan from 'morgan'
import Database from './config/db'
import { initUser } from './models/User'
import { initPackages } from './models/Packages'

app.use(helmet()); // set well-known security-related HTTP headers
app.use(compression());

app.disable("x-powered-by");

app.use(cors());
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

allRoutes(app);

const databaseInstance = new Database().database;

console.log('Initializing User');
initUser(databaseInstance);
console.log('Initializing Packages');
initPackages(databaseInstance);


const server = app.listen(3000, () =>
    console.log("Starting ExpressJS server on Port 3000"));

export default server;
