import {Sequelize} from 'sequelize';

export default class Database {

    db: string;
    user: string;
    password: string;
    host: string;
    port: number;
    database: Sequelize;

    constructor() {
        this.db = 'superadmin';
        this.user = 'root';
        this.password = '';
        this.host = 'localhost';
        this.port = 3306;

        this.database = new Sequelize(this.db, this.user, this.password, {
            host: this.host,
            dialect: 'mysql',
            port: this.port,
            logging: true
        })
        
        
        this.database.authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });

        this.database.sync({
            // Using 'force' will drop any table defined in the models and create them again.
            //  force: true
        })
    }
}