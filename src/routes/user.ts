import { Request, Response } from 'express';
import signupValidation from '../middlewares/user/signup.middleware';
import signinValidation from '../middlewares/user/signin.middleware';
import { changePasswordMiddleware } from '../middlewares/user'
import verify from '../middlewares/auth/verify.middleware'
import { register, signin, home, changePassword } from '../controllers/user';

const user = (app: any) => {
    const prefix = '/user';
    app.get(prefix , (req: Request, res: Response) => {
        return res.status(200).json({message: 'everything works fine!!'})
    })

    app.post(prefix + '/signup', signupValidation, register)
    app.post(prefix + '/signin', signinValidation, signin)
    app.get(prefix + '/home', verify, home)
    app.put(prefix + '/', verify, changePasswordMiddleware, changePassword)
}

export default user;