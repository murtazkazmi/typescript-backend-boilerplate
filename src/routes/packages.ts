import { Application } from 'express'
import { packageCreationMiddleware, packageDeletionMiddleware, packageUpdateMiddleware } from '../middlewares/packages'
import {
    createPackage,
    deletePackage,
    getAllPackages,
    undeletePackage,
    updatePackage
} from '../controllers/packages'


const packages = (app: Application) => {
    const prefix = '/packages';

    app.post(prefix + '/', packageCreationMiddleware, createPackage );
    app.delete(prefix + '/', packageDeletionMiddleware, deletePackage);
    app.put(prefix + '/undelete', packageDeletionMiddleware, undeletePackage)
    app.get(prefix + '/', getAllPackages);
    app.put(prefix + '/', packageUpdateMiddleware, updatePackage);

}

export default packages;