import user from './user'
import packages from './packages'
import {Request, Response, Application} from 'express'
const index = (app: Application) => {
    app.get('/',(req: Request, res: Response) => {
        return res.json({message: 'hello'});
    })
}


const allRoutes = (app: Application) => {
    return {
        root: index(app),
        home: user(app),
        packages: packages(app)
    }
}
export default allRoutes