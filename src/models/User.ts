import { Model, 
    DataTypes, 
    Sequelize} from 'sequelize';


export class User extends Model{
    public id!: number;
    public email!:  string;
    public password!:   string;
    public fullName!:   string;
    public phoneNumber!:    string;
    public lastLogin!: Date;
}

export const initUser = (sequelize: Sequelize) => {
    User.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        fullName: {
            type: DataTypes.STRING
        },
        phoneNumber: {
            type: DataTypes.STRING
        },
        lastLogin: {
            type: DataTypes.DATE
        }
    },{
        defaultScope: {
            attributes: { exclude: ['password']}
        },
        scopes: {
            findViaEmail(email) {
                return {
                    attributes: ['id','email','password','fullName','phoneNumber','lastLogin'],
                    where: {
                        email: email
                    }
                }
            },
            findById(id){
                return {
                    attributes: ['id','email','password','fullName','phoneNumber', 'lastLogin'],
                    where: {
                        id: id
                    }
                }
            }
        },
        sequelize: sequelize,
        tableName: 'User'
    })
}

