import { Model, DataTypes, Sequelize } from 'sequelize';




export class Packages extends Model{
    public id!: number;
    public packageName!: string;
    public price!:  number;
    public supportsMultipleFranchises!: Boolean;
    public maxNumberOfFranchises!: number;
    public support!: string;
    public mobileAppAvailable!: Boolean;
    public isDeleted!: Boolean;
    public isDiscounted!: Boolean;
    public discount!: number;
    public discountDescription!: string; 
}
export const initPackages = (sequelize: Sequelize) => {
    Packages.init({
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        packageName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        supportsMultipleFranchises: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        maxNumberOfFranchises: {
            type: DataTypes.INTEGER,
            defaultValue: 1
        },
        support:{
            type: DataTypes.STRING,
            allowNull: false
        },
        mobileAppAvailable:{
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isDiscounted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        discount: {
            type: DataTypes.FLOAT,
            defaultValue: 0
        },
        discountDescription: {
            type: DataTypes.STRING,
            defaultValue: ''
        }
    },{
        defaultScope: {
            where: {
                isDeleted: false
            }
        },
        scopes: {
            getDeleted(id){
                return {
                    where: {
                        id: id,
                        isDeleted: true
                    }
                }
            }
        },
        sequelize: sequelize,
        tableName: 'Packages'
    })
}
