import { Request, Response } from 'express';
import { Packages } from '../../models/Packages'


const createPackage = async(req:Request, res:Response) => {
    try{
        let packageCreated = await Packages.create(req.body);
        return res.status(201).json(packageCreated);
    } catch(err){
        res.status(500).json(err);
    }

}

const deletePackage = async(req:Request, res:Response) => {
    let packageToDelete = await Packages.findByPk(req.body.id);
    if(!packageToDelete){
        return res.status(404).json({message: 'No package found'});
    }
    packageToDelete.isDeleted = true;
    await packageToDelete.save();
    return res.status(200).json(packageToDelete);
}

const undeletePackage = async(req:Request, res:Response) => {
    let packageToUpdate = await Packages.scope({method: ['getDeleted',req.body.id]}).findOne();
    if(!packageToUpdate){
        return res.status(404).json({message: 'No package found'});
    }
    packageToUpdate.isDeleted = false;
    await packageToUpdate.save();
    return res.status(200).json(packageToUpdate);
}

const getAllPackages = async(req:Request, res:Response) => {
    let packages: any = await Packages.findAll();
    if(packages.length){
        return res.status(200).json(packages);
    }
    return res.status(204).json({message: "No active package"});
}

const updatePackage =async(req:Request, res:Response) => {
    let params = req.body;
    let packageFound = await Packages.findByPk(params.id);
    if(!packageFound){
        return res.status(404).json({message: "No package found to update"});
    }
    try{
        let packageToUpdate = await Packages.update(params,{where: {id: params.id}});
        return res.status(200).json(packageToUpdate);
    } catch(err) {
        return res.status(500).json(err)
    }
}

export {
    createPackage,
    deletePackage,
    undeletePackage,
    getAllPackages,
    updatePackage
}