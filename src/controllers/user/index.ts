import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
import * as jwt from 'jsonwebtoken'
import constants from '../../config/constants'
import { User } from '../../models/User'

const register = (req: Request, res: Response) => {
    try {
        let salt = 10;
        bcrypt.genSalt(salt, (err: any, saltGen: any) => {
            bcrypt.hash(req.body.password, saltGen, async (err: any, hash: any) => {
                let userObject = {
                    email: req.body.email,
                    password: hash,
                    fullName: req.body.fullName,
                    phoneNumber: req.body.phoneNumber
                }
                try {
                    let userCheck = await User.scope({ method: ['findViaEmail', userObject.email] }).findOne();
                    if (userCheck) {
                        return res.status(409).json({ message: "A user exists with this email already" });
                    } else {
                        let user = await User.create(userObject);
                        return res.status(201).json(user);
                    }
                } catch (exception) {
                    return res.status(500).json({ errorMessage: exception })
                }
            })
        })
    } catch (exception) {
        return res.status(500).json({ errorMessage: exception });
    }
}


const changePassword = async (req: Request, res: Response) => {
    let user = await User.scope({ method: ['findById', req.body.id] }).findOne();
    if (!user) {
        return res.status(401).json({ message: "Invalid user id" });
    }
    try {
        let match = await bcrypt.compare(req.body.oldPassword, user.password);
        if (!match) {
            return res.status(401).json({ message: "Invalid old password" });
        }
        if (req.body.oldPassword == req.body.newPassword) {
            return res.status(400).json({
                message: "New password can't be the same as old password"
            })
        }
        bcrypt.genSalt(10, (error, salt) => {
            bcrypt.hash(req.body.newPassword, salt, async (error, hash) => {
                if (error) {
                    return res.status(500).json(error);
                }
                await User.update({ password: hash }, { where: { id: req.body.id } });
                user = await User.scope({ method: ['findById', req.body.id] }).findOne();
                console.log(user)
                if (user) {
                    let token = await jwt.sign(JSON.stringify(user), constants.jwtSecret);
                    delete user.password;
                    return res.status(200).json({ user, token });
                }
                return res.status(401).json({ message: "id changed" });
            })
        })
    } catch (error) {
        return res.status(401).json({ message: "Invalid old password" });
    }
}

const signin = async (req: Request, res: Response) => {
    try {
        let user = await User.scope({ method: ['findViaEmail', req.body.email] }).findOne();
        if (user) {
            let result = await bcrypt.compare(req.body.password, user.password);
            if (result) {
                await User.update({ lastLogin: new Date() }, { where: { id: user.id } });
                user = await User.scope({ method: ['findViaEmail', req.body.email] }).findOne();
                let token = await jwt.sign(JSON.stringify(user), constants.jwtSecret);
                return res.status(200).json({ user, token });
            } else {
                return res.status(403).json({ message: 'Email/Password is wrong' })
            }
        } else {
            return res.status(403).json({ message: 'Email/Password is wrong' })
        }
    } catch (exception) {
        return res.status(500).send(exception);
    }
}

const home = async (req: Request, res: Response) => {
    let userName = req.body.user.fullName;
    return res.status(200).send(`hello ${userName}`);
}

export {
    register,
    signin,
    home,
    changePassword
}