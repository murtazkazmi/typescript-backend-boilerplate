import server from "../server";
import request from "supertest";
import {User} from '../models/User'

describe("GET / - a simple api endpoint", () => {
  it("Hello API Request", async (done) => {
    const result = await request(server).get("/");
    expect(result.body.message).toEqual("hello");
    expect(result.status).toEqual(200);
    done();
  });
});

describe("User register", () => {
  beforeAll(async () => {
    await User.destroy({
      where: {},
      truncate: true
    })
  })

  it("Should register a user", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "johndoe@gmail.com",
      password: "johndoe123",
      fullName: "john doe",
      phoneNumber: "0323-5232371"
    });
    console.log(result);
    expect(result.status).toEqual(201);
    done();
  })

  it("Should enter simple string in place of email and expect a 400 error", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "my name is John Doe",
      password: "johndoe123",
      fullName: "john doe",
      phoneNumber: "0323-5232371"
    });
    expect(result.status).toEqual(400);
    done();
  })

  it("Should enter spassword of length less than 8 and expect a 400 error", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "johndoe@gmail.com",
      password: "johndoe",
      fullName: "john doe",
      phoneNumber: "0323-5232371"
    });
    expect(result.status).toEqual(400);
    done();
  })

  it("Should miss full name from request body and expect 400 error", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "john@gmail.com",
      password: "johndoe123",
      phoneNumber: "0323-5232371"
    });
    expect(result.status).toEqual(400);
    done();
  })

  it("Should miss email from request body and expect 400 error", async (done) => {
    const result = await request(server).post("/user/signup").send({
      password: "johndoe123",
      fullName: "John Doe",
      phoneNumber: "0323-5232371"
    });
    expect(result.status).toEqual(400);
    done();
  })

  it("Should miss password from request body and expect 400 error", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "john@gmail.com",
      fullName: "John Doe",
      phoneNumber: "0323-5232371"
    });
    expect(result.status).toEqual(400);
    done();
  })

  it("Should miss phone number from request body and expect 400 error", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "john@gmail.com",
      fullName: "John Doe",
      password: "johndoe123"
    });
    expect(result.status).toEqual(400);
    done();
  })

  it("Should add a duplicate user and expect 409", async (done) => {
    const result = await request(server).post("/user/signup").send({
      email: "johndoe@gmail.com",
      password: "johndoe123",
      fullName: "john doe",
      phoneNumber: "0323-5232371"
    });
    expect(result.status).toEqual(409);
    done();
  })


})