import { Request, Response, NextFunction } from 'express';
import * as JOI from "@hapi/joi";


const changePasswordSchema = JOI.object().keys({
    id: JOI.number().required(),
    oldPassword: JOI.string().min(8).required(),
    newPassword: JOI.string().min(8).required()
}); 


const changePasswordMiddleware = (req:Request, res: Response, next:NextFunction) => {
    delete req.body.user;
    const result = changePasswordSchema.validate(req.body);
    console.log(result)
    if(result.error){
        return res.status(400).json(result.error);
    }
    next();
}

export {
    changePasswordMiddleware
}