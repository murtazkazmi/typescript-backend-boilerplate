import { Request, Response, NextFunction } from 'express';
import * as JOI from '@hapi/joi'

const packageCreationSchema = JOI.object().keys({
    packageName: JOI.string().required(),
    price: JOI.number().required(),
    supportsMultipleFranchise: JOI.boolean().default(false),
    maxNumberOfFranchises: JOI.number().default(1),
    support: JOI.string().required(),
    mobileApplicationAvailable: JOI.boolean().default(false),
    isDiscounted: JOI.boolean().default(false),
    discount: JOI.number().default(0),
    discountDescription: JOI.string().default('')
})

const packageUpdateSchema = JOI.object().keys({
    id: JOI.number(),
    packageName: JOI.string(),
    price: JOI.number(),
    supportsMultipleFranchise: JOI.boolean(),
    maxNumberOfFranchises: JOI.number(),
    support: JOI.string(),
    mobileApplicationAvailable: JOI.boolean(),
    isDiscounted: JOI.boolean(),
    discount: JOI.number(),
    discountDescription: JOI.string()
})


const packageCreationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const result = packageCreationSchema.validate(req.body);
    if (result.error) {
        return res.status(400).json(result.error);
    } else {
        next();
    }
}

const packageDeletionMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (!req.body.id || isNaN(req.body.id)) {
        return res.status(400).json({ message: 'Invalid request' });
    }
    next();
}

const packageUpdateMiddleware = (req:Request, res:Response, next: NextFunction) => {
    const result = packageUpdateSchema.validate(req.body);
    if(result.error){
        return res.status(400).json(result.error);
    }
    next();
}


export {
    packageCreationMiddleware,
    packageDeletionMiddleware,
    packageUpdateMiddleware
}