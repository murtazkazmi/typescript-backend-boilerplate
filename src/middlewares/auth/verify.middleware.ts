import { Request, Response, NextFunction } from 'express'
import * as JWT from 'jsonwebtoken'
import constants from '../../config/constants'
import { User } from '../../models/User'


const verify = (req: Request, res: Response, next: NextFunction) => {
    let authentication = req.headers.authorization;
    if(authentication){
        let accessToken = (authentication as string).split(' ')[1];
        JWT.verify(accessToken, constants.jwtSecret, async(err: any, decode: any) => {
            if(err){
                return res.status(401).json({message: 'Invalid token'});
            }
            let user: any = await User.scope({method: ['findById',decode.id]}).findOne();
            if(!user){
                return res.status(401).json({message: "User not found"});
            }
            console.log(user.password)
            console.log(decode.password)
            if(user.password != decode.password){
                return res.status(401).json({message: "Old token. Password mismatched"});
            }
            
            if(new Date(user.lastLogin).getTime() != new Date(decode.lastLogin).getTime()){
                return res.status(401).json({message: "OldToken"});
            }
            req.body.user = decode;
            next();
        });
    } else {
        return res.status(401).json({message: 'No token supplied'});
    }
}

export default verify