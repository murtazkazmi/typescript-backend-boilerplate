## Introduction

  

A typescript express boilerplate to get started. It uses yarn as package manager.

  

## Install

  

yarn

  

## Runing the code

  

yarn run start

  

## Testing the code

  

yarn run test

  

## Contains

 - Routes
 - Controllers
 - Middlewares
 - Models
 - Sequelize ORM
 - Basic Signup and Signin functionality
 - Basic test cases with JEST
